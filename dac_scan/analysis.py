import csv
import re
import sys
import matplotlib.pyplot as plt
import numpy as np

x_data = []
y_data = []

# define default values
default_values = {
    "dac_ibias": 32,
    "dac_itrim": 32,
    "dac_ikrum": 32,
    "dac_vthr": 70,
    "dac_vref": 70,
    "vtpulse": 0
}

# read filename provide as an argument
if len(sys.argv) != 2:
    print("Usage: python3 analysis.py <csv_filename>")
    sys.exit(1)

# extract the filename
csv_filename = sys.argv[1]

# read data from the csv file
with open(csv_filename, 'r') as csv_file:
    csv_reader = csv.reader(csv_file)
    header_lines = []

    # read header
    for line in csv_reader:
        if "scanned DAC" in line[0]:
            x_axis_line = line[0]
            break
        header_lines.append(line)

    # extract x label from the header
    match = re.search(r'"([^"]+)"', x_axis_line)
    if match:
        x_axis_label = match.group(1)
    else:
        x_axis_label = "Unknown X-axis"

    # read data
    for row in csv_reader:
        if len(row) == 2:  # ensure there are two columns of data
            x_data.append(int(row[0]))
            y_data.append(float(row[1]))

    # create lists to store means and RMS for each value in the first column
    values = list(set(x_data))
    mean_values = []
    std_dev_values = []

    # group same x values (and y values) and calculate the mean and RMS
    for value in values:
        group_y_data = [y for x, y in zip(x_data, y_data) if x == value]
        mean = np.mean(group_y_data)
        std_dev = np.std(group_y_data)
        mean_values.append(mean)
        std_dev_values.append(std_dev)


# Plot the mean value and RMS
plt.errorbar(values, mean_values, yerr=std_dev_values, fmt='o', label='measurement', markersize=1)
plt.xlabel(x_axis_label, fontsize=16)
plt.ylabel('Voltage [V]', fontsize=16)
plt.title(x_axis_label, fontsize=16)

# plot default value
default = default_values.get(x_axis_label, None)
if default is not None:
    plt.axvline(x=default, color='gray', linestyle='dashed', label="default DAC value")

plt.legend()
plt.show()
