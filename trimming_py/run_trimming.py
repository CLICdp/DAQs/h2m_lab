import logging
import numpy as np
from os import makedirs


class Equalisation:
    """
    Class for performing equalisation on a given input data.

    Args:
        path (str): The path to the input data.
        output_folder (str): The path to the output folder.
        threshold_range (list, optional): The range of threshold values. Defaults to [110, 30].
        threshold_step (int, optional): The step size for threshold values. Defaults to 1.
        matrix_size (list, optional): The size of the matrix. Defaults to [64, 16] (64 columns, 16 rows)
        trims (list, optional): The trim values. Defaults to [0, 15].
        testpulse (bool, optional): Flag indicating whether testpulse is enabled. Defaults to False.
    """

    def __init__(
        self,
        path,
        output_folder,
        threshold_range=[255, 0],
        threshold_step=1,
        matrix_size=[64, 16],
        trims=[0, 15],
        testpulse=False,
    ):
        self.path = path
        self.output_folder = output_folder
        self.threshold_step = threshold_step
        self.threshold_range = threshold_range
        self.threshold_count = None
        self.thresholds = np.array([])
        self.matrix_size = matrix_size
        self.trims = trims
        self.testpulse = testpulse

        self.mask = None
        self.trim_result = None

        logging.basicConfig(
            level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
        )

        self.calculateThresholds()

    def calculateThresholds(self):
        """
        Calculate the thresholds based on the given range and step size.

        This method checks for the correct order of the threshold range and calculates
        the threshold count. It then fills an appropriate matrix with the calculated thresholds.

        Returns:
            None
        """
        # check for correct order of threshold range
        if self.threshold_range[0] < self.threshold_range[1]:
            self.threshold_range = [self.threshold_range[1], self.threshold_range[0]]

        # calculate threshold count and fill appropriate matrix
        self.threshold_count = (
            int(
                (self.threshold_range[0] - self.threshold_range[1])
                / self.threshold_step
            )
            + 1
        )
        self.thresholds = np.linspace(
            self.threshold_range[1], self.threshold_range[0], self.threshold_count
        )[:, None, None] * np.ones(
            (self.threshold_count, self.matrix_size[0], self.matrix_size[1])
        )

    def produceEqualisation(self):
        """
        Perform equalisation by calculating per pixel threshold means and producing the final output.

        Returns:
            mask (ndarray): The final mask array.
            trim_result (ndarray): The configuration array with the final trim values.
            first_means_stdevs (list): A list containing the mean and standard deviation of the first trim.
            last_means_stdevs (list): A list containing the mean and standard deviation of the last trim.
        """
        means = []
        stdevs = []
        masks = []

        # process all trim files
        for trim in self.trims:
            filenames = [f"{self.path}/noise_scan_{trim}.bin"]
            print(filenames)

            mean, stdev, mask = self.processInput(filenames)
            means.append(mean)
            stdevs.append(stdev)

            # convert to 0/1 from False/True
            masks.append(mask.astype("uint16"))

        # calculate mask and trim arrays
        self.mask, self.trim_result = self.calculateTrim(means, self.trims)
        self.mask = np.sum(np.max(masks, axis=0) + [self.mask], axis=0, dtype="uint16")
        # the final mask is following:
        # mask==1 noise
        # mask==2 presumed dead
        # mask==4 needed < 0 to equalise
        # mask==8 needed > 15 to equalise
        # combinations are available by summing the values
        logging.debug(np.argwhere(self.mask))

        # produce final format to be saved
        result = self.produceOutput(self.mask, self.trim_result, self.matrix_size)
        self.saveOutput(self.output_folder, result)

        self.saveOutput(self.output_folder, self.trim_result, "trim")
        self.saveOutput(self.output_folder, self.mask, "mask")
        for mean, trim in zip(means, self.trims):
            self.saveOutput(self.output_folder, mean, f"mean{trim}")
        for stdev, trim in zip(stdevs, self.trims):
            self.saveOutput(self.output_folder, stdev, f"noise{trim}")

        logging.info(f"Finished equalisation")

        return self.mask, self.trim_result, [means[0], stdevs[0]], [means[-1], stdevs[-1]]

    def readBinaryFile(self, filename):
        """
        Reads a binary file and returns the data as a numpy array.

        Args:
            filename (str): The path to the binary file.

        Returns:
            numpy.ndarray: The data read from the binary file, represented as a numpy array.
        """

        logging.info(f"Reading binary file {filename}")
        # Open data file
        with open(filename, "rb") as data_in:
            # Get length of file
            data_in.seek(0, 2)
            length = data_in.tell()
            data_in.seek(0)  # Seek back to the beginning
            print(f"Reading {length} bytes")

            # prepare scan result in a matrix like format
            scan = np.zeros(
                (self.threshold_count, self.matrix_size[0], self.matrix_size[1]),
                dtype=np.uint16,
            )

            # Buffer for one block of testpulses
            n_col = self.matrix_size[0]
            n_row = self.matrix_size[1]
            blocksize = n_row * n_col + 6
            block = np.zeros(blocksize, dtype=np.uint16)

            # Reading data from binary file
            while True:
                data = data_in.read(2 * blocksize)
                if not data:
                    break

                block = np.frombuffer(data, dtype=np.uint16)

                threshold = block[n_col * n_row + 0]
                scan_start = block[n_col * n_row + 1]
                # scan_step = block[n_col * n_row + 2]
                # scan_end = block[n_col * n_row + 3]
                # scan_n = block[n_col * n_row + 4]
                # scan_dac_i = block[n_col * n_row + 5]

                for col in range(n_col):
                    for row in range(n_row):
                        scan[threshold - self.threshold_range[1], col, row] += block[
                            col + row * n_col
                        ]

                if threshold == scan_start:
                    continue
            return scan

    def processInput(self, filenames):
        """
        Process the input files and calculate the mean, standard deviation, and mask.

        Args:
            filenames (list): A list of file names to process.

        Returns:
            tuple: A tuple containing the mean, standard deviation, and mask.
                - mean (ndarray): The weighted average of the thresholds based on the scan result.
                - stdev (ndarray): The standard deviation of the pixel noise.
                - mask (ndarray): A meaningful mask to distinguish different types.

        """
        logging.info(f"Processing files {filenames}")

        # prepare scan result in a matrix like format
        scan = np.zeros(
            (self.threshold_count, self.matrix_size[0], self.matrix_size[1]),
            dtype=np.uint16,
        )

        # loop all files (needed in case of mask scan)
        for filename in filenames:
            scan = self.readBinaryFile(filename)

        mask_noise = np.count_nonzero(scan == 1, axis=0) >= self.threshold_count
        scan[:, mask_noise] = np.array([-1]).astype(np.uint16)

        # remove all single hit counts (to remove accumulated noise from averaging)
        scan[scan == 1] = 0

        mask_dead = np.count_nonzero(scan == 0, axis=0) >= self.threshold_count
        scan[:, mask_dead] = np.array([-1]).astype(np.uint16)

        # return weighted average of the threshold based on the scan result (pixel counts), stdev of pixel noise and mask of noisy channels
        mean = np.average(self.thresholds, weights=scan, axis=0)
        stdev = np.sqrt(np.average((self.thresholds - mean) ** 2, weights=scan, axis=0))

        # TODO: here one could improve by having a fit instead of just taking the mean and stddev

        mask_stdev = np.zeros_like(
            mean
        )  # FIXME should be based on stdev cut such as (stdev > 15)

        # make sure the mean and stdev is masked from noise
        mean[(mask_noise | mask_dead)] = np.nan
        stdev[(mask_noise | mask_dead)] = np.nan

        # return meaningful mask to distinguish different types
        mask = ((mask_noise) * 1) + (mask_dead * 2)

        return mean, stdev, mask

    def calculateTrim(self, means, trims, trim_range=[0, 15]):
        """
        Calculates the mask and trim values based on the given means and trims.

        Args:
            means (numpy.ndarray): Array of mean values.
            trims (numpy.ndarray): Array of trim values.
            trim_range (list, optional): Range of trim values. Defaults to [0, 15].

        Returns:
            tuple: A tuple containing the mask and trim values.
        """
        logging.info(f"Producing mask and trim results")

        # get target threshold
        target = np.nanmean(means[0] + means[-1]) / 2

        # calculate trim value as linear interpolation
        distance = means[-1] - means[0]
        distance[distance == 0] = np.inf
        trim = trims[0] + ((target - means[0]) * (trims[-1] - trims[0]) / distance)

        mask = np.zeros_like(trim)
        trim = np.rint(trim)  # make sure the trim values are properly rounded

        # mask as 4 if equalised less than trim min
        mask[trim < np.min(trim_range)] = 4
        trim[trim < np.min(trim_range)] = np.min(trim_range)

        # mask as 8 if equalised more than trim max
        mask[trim > np.max(trim_range)] = 8
        trim[trim > np.max(trim_range)] = np.max(trim_range)

        return mask, trim

    def produceOutput(self, mask, trim, matrix_size):
        """
        Produces the save format for the output.

        Args:
            mask (ndarray): The mask array.
            trim (ndarray): The trim array.
            matrix_size (tuple): The size of the matrix.

        Returns:
            ndarray: The result array in save format.
        """
        logging.info(f"Producing save format")

        x = np.tile(np.arange(matrix_size[0], dtype="uint16"), matrix_size[1])
        y = np.repeat(np.arange(matrix_size[1], dtype="uint16"), matrix_size[0])
        result = np.zeros((matrix_size[0] * matrix_size[1], 6), dtype="uint16")

        # only noisy pixels are masked (note: mask==1 noise, all other outcomes have even values)
        tmp_mask = mask.flatten()
        tmp_mask[tmp_mask % 2 == 1] = 1
        tmp_mask[tmp_mask > 1] = 0
        tmp_tp_enable = np.ones_like(tmp_mask)

        # check if trim contains nans
        tmp_trim = trim.T.flatten()
        if np.isnan(tmp_trim).any():
            tmp_trim[np.isnan(tmp_trim)] = 0
        tmp_trim = tmp_trim.astype("uint16")

        result[(x * matrix_size[1]) + y, 0:5] = np.array(
            [x, y, tmp_tp_enable, tmp_mask, tmp_trim]
        ).T.astype("uint16")
        return result

    def saveOutput(self, path, result, resulttype="eq", testpulse=1):
        """
        Save the output data to a file.

        Args:
            path (str): The path to the output folder.
            result (ndarray): The result data to be saved.
            resulttype (str, optional): The type of result data. Defaults to "eq".
            testpulse (int, optional): The test pulse value. Defaults to 0.
        """
        logging.info(f"Saving data {resulttype}")
        makedirs(self.output_folder, exist_ok=True)

        if resulttype == "eq":
            result[:, 2] = testpulse
            np.savetxt(
                f"{path}/matrix_{resulttype}.cfg",
                result,
                # header="Peary H2M Matrix configuration\nCOL ROW tp_enable mask tuning_dac not_top_pixel",
                header="COL ROW tp_enable mask tuning_dac not_top_pixel",
                fmt="%d",
            )
        else:
            np.savetxt(
                f"{path}/{resulttype}.csv",
                result,
                header=f"Peary H2M {resulttype} results",
                fmt="%.2f",
                delimiter=",",
            )

    def plot(self):
        """
        Plot the mask and trim using matplotlib.

        Parameters:
        - None

        Returns:
        - None
        """
        import matplotlib.colors as colors

        self.plot2D(
            self.mask,
            "mask",
            zlabel="mask",
            tag=True,
            norm=colors.BoundaryNorm(
                boundaries=np.linspace(1, 16, 16), ncolors=20, extend="both"
            ),
            cmap="tab20",
        )
        self.plot2D(self.trim_result, "trim", zlabel="trim")

    def plot2D(self, data, filename="", zlabel=None, tag=False, **kwargs):
        """
        Plot a 2D heatmap.

        Args:
            data (ndarray): The 2D data array to be plotted.
            filename (str, optional): The filename for saving the plot. Defaults to "".
            zlabel (str, optional): The label for the colorbar. Defaults to None.
            tag (bool, optional): Whether to add a tag with mask information. Defaults to False.
            **kwargs: Additional keyword arguments to be passed to plt.pcolormesh.

        Returns:
            None
        """
        import matplotlib.pyplot as plt
        plt.rcParams["font.size"] = 28
        plt.rcParams["figure.constrained_layout.use"] = True
        logging.info(f"Producing plot {filename}_2D")
        X, Y = np.meshgrid(
            np.arange(self.matrix_size[1]), np.arange(self.matrix_size[0])
        )

        fig = plt.figure(figsize=(8.5, 7), constrained_layout=True)
        fig.patch.set_alpha(0.0)
        plt.pcolormesh(X, Y, data, shading="nearest", **kwargs)
        plt.colorbar(label=zlabel)

        plt.xlabel("column [pixel]")
        plt.ylabel("row [pixel]")

        if tag:
            plt.text(
                10,
                120,
                "".join(
                    [
                        f"mask({d:.0f}) = {np.count_nonzero(data==d):.0f}\n"
                        for d in np.unique(data)
                    ]
                )[:-1],
                fontsize=14,
                verticalalignment="top",
                bbox=dict(boxstyle="round", facecolor="white", alpha=0.5),
            )

        if len(filename) > 0:
            fig.savefig(f"{self.output_folder}/{filename}_2D.png")
        else:
            plt.show()

    def plot1D(self, data, labels, path='', filename='', vline=False, xlabel=None, ylabel=None, yscale='linear', yscale_factor=1.4, **kwargs):
        import matplotlib.pyplot as plt
        plt.rcParams["font.size"] = 28
        plt.rcParams["figure.constrained_layout.use"] = True

        logging.info(f"Producing plot {filename}_1D")
        fig = plt.figure(figsize=(10, 10), constrained_layout=True)  # Increase the height of the figure
        fig.patch.set_alpha(0.0)

        for d, l in zip(data, labels):
            try:
                plt.hist(d.flatten(), label=f'{l}, $\mu$ = {np.nanmean(d):.2f}, $\sigma$ = {np.nanstd(d):.2f}', **kwargs)
                if vline:
                    plt.axvline(np.nanmean(d), ls='--', color='black')
            except:
                pass
        plt.grid()
        try:
            plt.legend(labelcolor="black")
        except:
            pass

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.yscale(yscale)
        plt.ylim(top=plt.ylim()[1] * yscale_factor)  # Scale the y-axis with the factor
        if len(filename) > 0:
            fig.savefig(f"{path}/{filename}.png")
        else:
            plt.show()

    def loadOutput(self, fname):
        return np.loadtxt(f"{self.output_folder}/{fname}.csv", delimiter=',')

    def loadEqualisation(self):
        self.trim_result   = self.loadOutput('trim').astype('uint16')
        self.mask   = self.loadOutput('mask').astype('uint16')
        mean0  = self.loadOutput('mean0')
        meanF  = self.loadOutput('mean15')
        stdev0 = self.loadOutput('noise0')
        stdevF = self.loadOutput('noise15')
        return [mean0, stdev0], [meanF, stdevF]

    def verifyEqualisation(self, filename):
        # verify the equalisation from the final scan
        mean, stdev, _ = self.processInput([filename])
        res  = [mean , stdev ]
        res0, resF = self.loadEqualisation()

        self.plot1D([res0[0], resF[0], res[0]], ["trim0", "trimF", "final"], self.output_folder, 'scan' , vline = True, range = (np.min(self.threshold_range), np.max(self.threshold_range)), bins = int(np.abs(np.diff(self.threshold_range))/self.threshold_step), xlabel="DAC", ylabel="#pixels")

# Usage example:
eq = Equalisation(
    "/Users/pgadow/Coding/ep-rd/h2m/h2m_lab/trimming_py/data/H2M-5/2024-04-08-defaultsettings",
    "output_folder",
    threshold_range=[256, 0],
    threshold_step=1,
    matrix_size=[64,16], # columns, rows
    trims=[0, 15],
    testpulse=False,
)
eq.produceEqualisation()
eq.plot()
# run this after the mask has been produced and a final scan has been performed
tuned_result_ready = True
if tuned_result_ready:
    eq.verifyEqualisation("/Users/pgadow/Coding/ep-rd/h2m/h2m_lab/trimming_py/data/H2M-5/2024-04-08-defaultsettings/tuned_result.bin")
