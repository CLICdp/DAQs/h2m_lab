import uproot
import numpy as np
import matplotlib.pyplot as plt


conversionFactor = 56.3 # Modidy! from electrons to DAC
# Open the ROOT file
file_name = "data/result.root"
with uproot.open(file_name) as file:
    # Get plots
    hist2d = file["all_pixels_2D_response"]
    mean_graph = file["mean_pixel_response"]

    # Extract data for the 2D plot
    hist_values = hist2d.values()
    x_edges = hist2d.axis(0).edges()
    y_edges = hist2d.axis(1).edges()

    # Mask the data to limit z-values to a maximum of 500 and remove zero values (to avoid everything being blue)
    masked_values = np.clip(hist_values, 0, 500)
    masked_values[masked_values == 0] = np.nan  # Set zero values to NaN for transparency

    # Extract data for the mean plot
    mean_x = mean_graph.values()[0]  # x-axis data
    mean_y = mean_graph.values()[1]  # y-axis data

    # Plot
    plt.figure(figsize=(8, 6))
    plt.plot(mean_x, mean_y, 'r-', marker='o', markersize=0.2, label='Mean ToT')
    c = plt.pcolormesh(x_edges, y_edges, masked_values.T, shading='auto', cmap='viridis', vmin=0)
    cbar = plt.colorbar(c)
    cbar.set_label("pixels", fontsize=16)
    plt.xlabel("dac_vtpulse [DAC]", fontsize=16)
    plt.ylabel("ToT [clock cycles]", fontsize=16)
    plt.legend(loc='lower right')
    plt.xlim(0, 253)
    plt.ylim(0, 110)

    # Add secondary x-axis

    def dac_to_electron(x):
        return x * conversionFactor

    def electron_to_dac(x):
        return x / conversionFactor

    secax = plt.gca().secondary_xaxis('top', functions=(dac_to_electron, electron_to_dac))
    secax.set_xlabel("amplitude [$e^-$]", fontsize=16)
    secax.set_xlim(dac_to_electron(0), dac_to_electron(253))

    plt.show()
