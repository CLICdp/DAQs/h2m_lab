import uproot
import numpy as np
import matplotlib.pyplot as plt

################################################################################
# Parameters to modify
baseline = 94
calibration_factor = 40.2
################################################################################

# Open the ROOT file
file_name = "data/result_new.root"
with uproot.open(file_name) as file:
    # Get plots
    hist2d = file["all_pixels_2D_response"]
    mean_graph = file["mean_pixel_response"]

    # Extract data for the 2D plot
    hist_values = hist2d.values()
    x_edges = hist2d.axis(0).edges()
    y_edges = hist2d.axis(1).edges()

    # Mask the data to limit z-values to a maximum of 500 and remove zero values
    masked_values = np.clip(hist_values, 0, 500)
    masked_values[masked_values == 0] = np.nan  # Set zero values to NaN for transparency

    # Extract data for the mean plot
    mean_x = mean_graph.values()[0]  # x-axis data
    mean_y = mean_graph.values()[1]  # y-axis data

    # Transform mean_y to amplitude in e-
    amplitude_e = (mean_y - baseline) * calibration_factor

    # Linear fit between x = 25 and x = 75 using the second y-axis (amplitude in e-)
    mask = (mean_x >= 25) & (mean_x <= 75)
    slope, intercept = np.polyfit(mean_x[mask], amplitude_e[mask], 1)
    fit_line = slope * mean_x[mask] + intercept

    # Create the plot
    fig, ax1 = plt.subplots(figsize=(8, 6))

    # Plot the left y-axis
    ax1.plot(mean_x, mean_y, 'r-', marker='o', markersize=0.2, label='Mean amplitude')
    ax1.set_xlabel("dac_vtpulse [DAC]", fontsize=16)
    ax1.set_ylabel("amplitude [THL DAC]", fontsize=16)
    ax1.tick_params(axis='y')
    ax1.set_xlim(0, 253)
    ax1.set_ylim(75, 265)

    # Plot the 2D histo
    c = ax1.pcolormesh(x_edges, y_edges, masked_values.T, shading='auto', cmap='viridis', vmin=0)

    # Adjust layout
    plt.subplots_adjust(right=1)
    cbar = plt.colorbar(c, ax=ax1, pad=0.15)
    cbar.set_label("pixels", fontsize=16)

    # Create a second y-axis on the right for amplitude in e-
    ax2 = ax1.twinx()
    ax2.set_ylabel("amplitude [e-]", fontsize=16, labelpad=15)  # Add padding to avoid overlap
    ax2.tick_params(axis='y')
    ax2.set_ylim((75 - baseline) * calibration_factor, (265 - baseline) * calibration_factor)

    # Plot the second y-axis
    ax2.plot(mean_x, amplitude_e, 'r--', marker='o', markersize=0.2, label='Mean amplitude')

    # Print fit paramameters
    ax2.text(0.95, 0.2, f"slope = {slope:.1f} $e^-$ / DAC", color='orange', fontsize=12,
             verticalalignment='bottom', horizontalalignment='right', transform=ax2.transAxes)

    plt.legend(loc='lower right')

    plt.show()
