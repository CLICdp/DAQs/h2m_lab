import ROOT
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.optimize import curve_fit

################################################################################
#Modify the following parameters:

# Threshold range of data recorded
start_index = 120
end_index = 170

# Baseline
baseline = 94

# Expected peak positions for the fits
min_peak_2 = 142
max_peak_2 = 149
min_peak_1 = 128
max_peak_1 = 136

# Datapath where the data is stored:
datapath = f"data_analysed_200frames_400times_10ms_long_ikrum10_1V2_h2m3"
################################################################################

# Initialize vectors
thresholds = []
pixel_count_sums = []
errors = []

# Loop over the file indices
for i in range(start_index, end_index + 1):
    filename = f"{datapath}/occupancy_filename_{i}.root"

    if not os.path.exists(filename):
        print(f"Warning: File {filename} does not exist. Skipping...")
        continue

    root_file = ROOT.TFile.Open(filename)

    histogram = root_file.Get("Pixel Count Sum")

    if not histogram:
        print(f"Warning: Histogram 'Pixel Count Sum' not found in {filename}. Skipping...")
        root_file.Close()
        continue

    counts = histogram.Integral()
    error = np.sqrt(counts)

    thresholds.append(i)
    pixel_count_sums.append(counts)
    errors.append(error)

    root_file.Close()

thresholds = np.array(thresholds)
pixel_count_sums = np.array(pixel_count_sums)
errors = np.array(errors)

derivative = np.gradient(pixel_count_sums, thresholds)
second_derivative = np.gradient(derivative, thresholds)


# Define error function
def error_func(x, L, x0, sigma, b):
    k = 1 / (sigma * np.sqrt(2 * np.log(2))) # compatible interpretation of sigma with the gaussian fit
    return L / (1 + np.exp(-k * (x - x0))) + b

# Fit ranges
error_func_fit_mask_peak1 = (thresholds >= min_peak_1) & (thresholds <= max_peak_1)
error_func_fit_mask = (thresholds >= min_peak_2) & (thresholds <= max_peak_2)

fit_thresholds_error_func_peak1 = thresholds[error_func_fit_mask_peak1]
fit_pixel_counts_error_func_peak1 = pixel_count_sums[error_func_fit_mask_peak1]
fit_thresholds_error_func = thresholds[error_func_fit_mask]
fit_pixel_counts_error_func = pixel_count_sums[error_func_fit_mask]

# Initial guesses
initial_guess_error_func_peak1 = [np.max(fit_pixel_counts_error_func_peak1), np.mean(fit_thresholds_error_func_peak1), 1.0, np.min(fit_pixel_counts_error_func_peak1)]
initial_guess_error_func = [np.max(fit_pixel_counts_error_func), np.mean(fit_thresholds_error_func), 1.0, np.min(fit_pixel_counts_error_func)]

# Fit the error function
try:
    popt_error_func_peak1, pcov_error_func_peak1 = curve_fit(error_func, fit_thresholds_error_func_peak1, fit_pixel_counts_error_func_peak1, p0=initial_guess_error_func_peak1)
    popt_error_func, pcov_error_func = curve_fit(error_func, fit_thresholds_error_func, fit_pixel_counts_error_func, p0=initial_guess_error_func)

    L_fit_error_func_peak1, x0_fit_error_func_peak1, sigma_fit_error_func_peak1, b_fit_error_func_peak1 = popt_error_func_peak1
    L_fit_error_func, x0_fit_error_func, sigma_fit_error_func, b_fit_error_func = popt_error_func

    perr_error_func_peak1 = np.sqrt(np.diag(pcov_error_func_peak1))
    perr_error_func = np.sqrt(np.diag(pcov_error_func))

    # Generate the curves for plotting
    x_fit_error_func_peak1 = np.linspace(min_peak_1, max_peak_1, 300)
    y_fit_error_func_peak1 = error_func(x_fit_error_func_peak1, *popt_error_func_peak1)

    x_fit_error_func = np.linspace(min_peak_2, max_peak_2, 300)
    y_fit_error_func = error_func(x_fit_error_func, *popt_error_func)

    # First plot: Counts vs Threshold with error bars
    plt.figure()
    plt.errorbar(thresholds, pixel_count_sums, yerr=errors, marker='o', linestyle='', capsize=5, color='black')
    plt.plot(x_fit_error_func_peak1, y_fit_error_func_peak1, color='orange', linestyle='--', label='s-curve fit')
    plt.plot(x_fit_error_func, y_fit_error_func, color='red', linestyle='--', label='s-curve fit')
    plt.xlabel('Threshold [DAC]', fontsize=16)
    plt.ylabel('Counts', fontsize=16)
    plt.xticks(range(start_index, end_index + 1, 5))

    # Print fit parameters
    text = '\n'.join((
        r'$\mu=%.1f \pm %.1f$' % (x0_fit_error_func_peak1, perr_error_func_peak1[1]),
        r'$\sigma=%.1f \pm %.1f$' % (2*sigma_fit_error_func_peak1, perr_error_func_peak1[2])))
    textstr_error_func = '\n'.join((
        r'$\mu=%.1f \pm %.1f$' % (x0_fit_error_func, perr_error_func[1]),
        r'$\sigma=%.1f \pm %.1f$' % (2*sigma_fit_error_func, perr_error_func[2])))
    plt.text(0.95, 0.7, text, transform=plt.gca().transAxes, fontsize=12,
             verticalalignment='bottom', horizontalalignment='right', color='orange')
    plt.text(0.95, 0.55, textstr_error_func, transform=plt.gca().transAxes, fontsize=12,
             verticalalignment='bottom', horizontalalignment='right', color='red')

    plt.legend()
    plt.tight_layout()

    print("Peak 1:")
    print(f"L: {L_fit_error_func_peak1:.2f} ± {perr_error_func_peak1[0]:.2f}")
    print(f"Mean (μ): {x0_fit_error_func_peak1:.2f} ± {perr_error_func_peak1[1]:.2f}")
    print(f"Sigma (σ): {sigma_fit_error_func_peak1:.2f} ± {perr_error_func_peak1[2]:.2f}")
    print(f"Offset (b): {b_fit_error_func_peak1:.2f} ± {perr_error_func_peak1[3]:.2f}")

    print("Peak 2:")
    print(f"L: {L_fit_error_func:.2f} ± {perr_error_func[0]:.2f}")
    print(f"Mean (μ): {x0_fit_error_func:.2f} ± {perr_error_func[1]:.2f}")
    print(f"Sigma (σ): {sigma_fit_error_func:.2f} ± {perr_error_func[2]:.2f}")
    print(f"Offset (b): {b_fit_error_func:.2f} ± {perr_error_func[3]:.2f}")

except Exception as e:
    print(f"Fit failed: {e}")
# Second plot: -dCounts/dThreshold vs Threshold
plt.figure()
plt.plot(thresholds, -derivative, marker='o', linestyle='-', color='black')
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('-dCounts/dThreshold', fontsize=16)
plt.xticks(range(start_index, end_index + 1, 5))

# Define Gaussian function
def gaussian(x, amplitude, mean, sigma):
    return amplitude * np.exp(- (x - mean)**2 / (2 * sigma**2))

# Gaussian fit for two regions
gaussian_fit_mask_1 = (thresholds >= min_peak_1) & (thresholds <= max_peak_1)
gaussian_fit_mask_2 = (thresholds >= min_peak_2) & (thresholds <= max_peak_2)

fit_thresholds_gaussian_1 = thresholds[gaussian_fit_mask_1]
fit_derivative_gaussian_1 = -derivative[gaussian_fit_mask_1]
fit_thresholds_gaussian_2 = thresholds[gaussian_fit_mask_2]
fit_derivative_gaussian_2 = -derivative[gaussian_fit_mask_2]

initial_guess_gaussian_1 = [np.max(fit_derivative_gaussian_1), fit_thresholds_gaussian_1[np.argmax(fit_derivative_gaussian_1)], 1.0]
initial_guess_gaussian_2 = [np.max(fit_derivative_gaussian_2), fit_thresholds_gaussian_2[np.argmax(fit_derivative_gaussian_2)], 1.0]

try:
    popt_gaussian_1, pcov_gaussian_1 = curve_fit(gaussian, fit_thresholds_gaussian_1, fit_derivative_gaussian_1, p0=initial_guess_gaussian_1)
    popt_gaussian_2, pcov_gaussian_2 = curve_fit(gaussian, fit_thresholds_gaussian_2, fit_derivative_gaussian_2, p0=initial_guess_gaussian_2)

    amplitude_fit_1, mean_fit_1, sigma_fit_1 = popt_gaussian_1
    amplitude_fit_2, mean_fit_2, sigma_fit_2 = popt_gaussian_2

    perr_gaussian_1 = np.sqrt(np.diag(pcov_gaussian_1))
    perr_gaussian_2 = np.sqrt(np.diag(pcov_gaussian_2))

    x_fit_gaussian_1 = np.linspace(min_peak_1, max_peak_1, 300)
    y_fit_gaussian_1 = gaussian(x_fit_gaussian_1, *popt_gaussian_1)

    x_fit_gaussian_2 = np.linspace(min_peak_2, max_peak_2, 300)
    y_fit_gaussian_2 = gaussian(x_fit_gaussian_2, *popt_gaussian_2)

    plt.plot(x_fit_gaussian_1, y_fit_gaussian_1, color='orange', linestyle='--', label='Gaussian fit')
    plt.plot(x_fit_gaussian_2, y_fit_gaussian_2, color='red', linestyle='--', label='Gaussian fit')

    plt.legend()
    textstr_gaussian_1 = '\n'.join((
        r'$\mu=%.1f \pm %.1f$' % (mean_fit_1, perr_gaussian_1[1]),
        r'$\sigma=%.1f \pm %.1f$' % (sigma_fit_1, perr_gaussian_1[2])))
    textstr_gaussian_2 = '\n'.join((
        r'$\mu=%.1f \pm %.1f$' % (mean_fit_2, perr_gaussian_2[1]),
        r'$\sigma=%.1f \pm %.1f$' % (sigma_fit_2, perr_gaussian_2[2])))
    plt.text(0.95, 0.7, textstr_gaussian_1, transform=plt.gca().transAxes, fontsize=12,
             verticalalignment='bottom', horizontalalignment='right', color='orange')
    plt.text(0.95, 0.55, textstr_gaussian_2, transform=plt.gca().transAxes, fontsize=12,
             verticalalignment='bottom', horizontalalignment='right', color='red')

    print("Gaussian Fit 1 Parameters (min_peak_1-max_peak_1):")
    print(f"Amplitude (A): {amplitude_fit_1:.2f} ± {perr_gaussian_1[0]:.2f}")
    print(f"Mean (μ): {mean_fit_1:.2f} ± {perr_gaussian_1[1]:.2f}")
    print(f"Sigma (σ): {sigma_fit_1:.2f} ± {perr_gaussian_1[2]:.2f}")

    print("Gaussian Fit 2 Parameters (min_peak_2-max_peak_2):")
    print(f"Amplitude (A): {amplitude_fit_2:.2f} ± {perr_gaussian_2[0]:.2f}")
    print(f"Mean (μ): {mean_fit_2:.2f} ± {perr_gaussian_2[1]:.2f}")
    print(f"Sigma (σ): {sigma_fit_2:.2f} ± {perr_gaussian_2[2]:.2f}")

except Exception as e:
    print(f"Gaussian fit failed: {e}")

# Third plot: d²Counts/d²Threshold vs Threshold
plt.figure()
plt.plot(thresholds, second_derivative, marker='o', linestyle='-', color='black')
plt.axhline(y=0, color='gray', linestyle='--')
plt.axvline(x=mean_fit_1, color='orange', linestyle='--')
plt.axvline(x=mean_fit_2, color='red', linestyle='--')
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('d²Counts/d²Threshold', fontsize=16)
plt.xticks(range(start_index, end_index + 1, 5))

# Fourth plot: Energy [electrons] vs Threshold [DAC]
k_alpha = 5890 / 3.66  # k_alpha /Ee/h
x_points = [baseline, mean_fit_1]
y_points = [0, k_alpha]
x_points_error = [1.43, 0.1]
y_points_error = [3, 14]

x_points_second_peak = [baseline, mean_fit_2]


plt.figure()
plt.errorbar(x_points_second_peak, y_points, xerr=x_points_error, yerr=y_points_error, fmt='o', capsize=5, color='black')
plt.errorbar(x_points, y_points, xerr=x_points_error, yerr=y_points_error, fmt='o', capsize=5, color='black')

# Linear fit
def linear_fit(x, slope, intercept):
    return slope * x + intercept

popt_linear, pcov_linear = curve_fit(linear_fit, x_points, y_points)
slope, intercept = popt_linear
perr_linear = np.sqrt(np.diag(pcov_linear))

popt_linear_second_peak, pcov_linear_second_peak = curve_fit(linear_fit, x_points_second_peak, y_points)
slope_second_peak, intercept_second_peak = popt_linear_second_peak
perr_linear_second_peak = np.sqrt(np.diag(pcov_linear_second_peak))

# Generate line for the fit
x_fit_linear = np.linspace(min(x_points), max(x_points), 300)
y_fit_linear = linear_fit(x_fit_linear, slope, intercept)
x_fit_linear_second_peak = np.linspace(min(x_points_second_peak), max(x_points_second_peak), 300)
y_fit_linear_second_peak = linear_fit(x_fit_linear_second_peak, slope_second_peak, intercept_second_peak)

# Plot the linear fit
plt.plot(x_fit_linear_second_peak, y_fit_linear_second_peak, linestyle='--', color='red', label=f'Linear fit, peak 1')
plt.plot(x_fit_linear, y_fit_linear, linestyle='--', color='orange', label=f'Linear fit, peak 2')

# Print values
textstr = r'$slope=%.0f \pm 1 \, e^-/DAC$' % (slope)
textstr_second_peak = r'$slope=%.0f \pm %.0f \, e^-/DAC$' % (slope_second_peak, perr_linear_second_peak[0])

plt.text(0.05, 0.75, textstr, transform=plt.gca().transAxes, fontsize=12,
         verticalalignment='top', horizontalalignment='left', color='orange')
plt.text(0.05, 0.65, textstr_second_peak, transform=plt.gca().transAxes, fontsize=12,
              verticalalignment='top', horizontalalignment='left', color='red')

plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('Energy [$e^-$]', fontsize=16)
plt.legend()

plt.show() # All plots
