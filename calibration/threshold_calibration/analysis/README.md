Analyse all the files in ```data``` using:

```
python3 run_analysis.py
```

And plot the threshold scan using:

```
python3 plotting.py
```
