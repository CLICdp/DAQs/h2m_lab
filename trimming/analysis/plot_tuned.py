import uproot
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit 

def gaus(x, norm,mean, sigma):
    return norm*np.exp(-0.5*((x-mean)/sigma)**2)

def make_plot(plot_name):
    # Open ROOT files and get histograms
    h_tuned = uproot.open("workdir/tuned_masked_result.root")[plot_name]
    h_small = uproot.open("workdir/noise_scan_0.root")[plot_name]
    h_large = uproot.open("workdir/noise_scan_15.root")[plot_name]

    # Convert uproot histograms to NumPy arrays
    values_tuned, bin_edges_tuned = h_tuned.to_numpy()
    values_small, bin_edges_small = h_small.to_numpy()
    values_large, bin_edges_large = h_large.to_numpy()

    # Calculate bin centers
    bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
    bin_centers_small = (bin_edges_small[:-1] + bin_edges_small[1:]) / 2
    bin_centers_large = (bin_edges_large[:-1] + bin_edges_large[1:]) / 2

    # Create a plot using matplotlib
    fig = plt.figure(figsize=(8, 6)) # Modify for different desired figsize

    # Plot histograms
    plt.plot(bin_centers_tuned, values_tuned, color='red', label='Equalised', drawstyle='steps-mid', zorder=2)
    plt.plot(bin_centers_small, values_small, color='darkmagenta', label='Lowest Trim Value', drawstyle='steps-mid', zorder=2)
    plt.plot(bin_centers_large, values_large, color='seagreen', label='Highest Trim Value', drawstyle='steps-mid', zorder=2)

    # Fill the area under the curves
    plt.fill_between(bin_centers_tuned, 0, values_tuned, color='red', alpha=1, zorder=1, step='mid')
    plt.fill_between(bin_centers_small, 0, values_small, color='darkmagenta', alpha=1, zorder=1, step='mid')
    plt.fill_between(bin_centers_large, 0, values_large, color='seagreen', alpha=1, zorder=1, step='mid')

    # Fit middle peak and draw
    guess_norm = max(values_tuned)
    guess_mean = np.average(bin_centers_tuned, weights=values_tuned)
    guess_sigma = np.average((bin_centers_tuned - guess_mean)**2, weights=values_tuned)
    guesses = [guess_norm, guess_mean, guess_sigma]
    pars, _ = curve_fit(gaus, bin_centers_tuned, values_tuned, guesses)
    fit_norm, fit_mean, fit_sigma = pars[0], pars[1], pars[2]

    # get uncertainty on fit_mean and fit_sigma
    perr = np.sqrt(np.diag(_))
    fit_mean_err, fit_sigma_err = perr[1], perr[2]

    # Plot the fitted Gaussian
    # fit_y = gaus(bin_centers_tuned, fit_norm, fit_mean, fit_sigma)
    # plt.plot(bin_centers_tuned, fit_y, '-', label=r'Fit ($\mu$ = 'f'{fit_mean:.1f}'+r', $\sigma$ = '+f'{fit_sigma:.2f})')

    # Plot the fitted Gaussian function as a histogram
    fit_y_hist = gaus(bin_centers_tuned, fit_norm, fit_mean, fit_sigma)
    plt.step(bin_centers_tuned, fit_y_hist, where='mid', label=r'Fit ($\mu$ = 'f'{fit_mean:.1f}'+r', $\sigma$ = '+f'{fit_sigma:.2f})', color='blue', linewidth=2)

    # Set axis labels
    plt.xlabel('Threshold [DAC]', fontsize=16)
    plt.ylabel('Frequency', fontsize=16)

    # Set axis limits
    plt.ylim(0, plt.ylim()[1])  # Set y-axis limit to start at 0
    plt.xlim(60, 140)  # Set x-axis limit from 30 to 110. Modify for different baselines.

    # Add legend
    plt.legend(loc='upper right', fontsize='large')

    # Show the plot
    plt.savefig(f'workdir/{plot_name.replace(" ","_").replace("/","_").lower()}.pdf')

    # Save fit parameters to a text file
    with open(f'workdir/{plot_name.replace(" ","_").replace("/","_").lower()}_fit.txt', 'w') as f:
        f.write(f'Fit Mean: {fit_mean:.2f} ± {fit_mean_err:.2f}\n')
        f.write(f'Fit Sigma: {fit_sigma:.2f} ± {fit_sigma_err:.2f}\n')


if __name__ == "__main__":

    plot_names = ["Pixel Count Sum", "threshold"]
    for p in plot_names:
        make_plot(p)
