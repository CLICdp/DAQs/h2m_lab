import uproot
import mplhep as hep
import re
import matplotlib.pyplot as plt

import sys

# Check if the file path is provided as a command line argument
if len(sys.argv) > 1:
    file_path = sys.argv[1]
else:
    print("Please provide the file path as a command line argument.")
    sys.exit(1)

# Use the file_path variable in your code
# ...

calibrate = True

# Open the root file
file = uproot.open(file_path)

# Extract device name, bias voltage, and krummenacher current from the file path
try:
    match = re.search(r'H2M-(\w+)_([\dp]+V)_([\w\d]+)', file_path)
    device_name = match.group(1)
    bias_voltage = match.group(2)
    krummenacher_current = match.group(3)
except AttributeError:
    device_name = bias_voltage = krummenacher_current = "Unknown"

calibration = {
    'H2M-2_1p2V_ikrum21': 27.279,
    'H2M-2_3p6V_ikrum21': 27.347,
    'H2M-3_1p2V_ikrum21': 27.167,
    'H2M-3_3p6V_ikrum21': 27.918,
    'H2M-4_1p2V_ikrum21': 25.098,
    'H2M-4_3p6V_ikrum21': 26.230,
    'H2M-5_1p2V_ikrum21': 26.745,
    'H2M-5_3p6V_ikrum21': 24.996,
    'H2M-2_1p2V_ikrum10': 26.532,
    'H2M-2_3p6V_ikrum10': 26.983,
    'H2M-3_1p2V_ikrum10': 26.551,
    'H2M-3_3p6V_ikrum10': 27.600,
    'H2M-4_1p2V_ikrum10': 0.0000,
    'H2M-4_3p6V_ikrum10': 0.0000,
    'H2M-5_1p2V_ikrum10': 25.865,
    'H2M-5_3p6V_ikrum10': 26.325,
}

calibration_factor = calibration[f'H2M-{device_name}_{bias_voltage}_{krummenacher_current}'] if calibrate else 1.

# Get the histogram
histogram = file["noise"]

# Get the histogram values and bin edges
values, bin_edges = histogram.to_numpy()

# Multiply the bin edges by the calibration factor
bin_edges *= calibration_factor

# Use the mplhep style
hep.style.use("ATLAS")

# Create a figure and axes
fig, ax = plt.subplots()

# Plot the histogram using mplhep
# hep.histplot(values, bins, ax=ax, histtype='fill', color='blue', label='Noise', flow="sum")
hep.histplot(values, bin_edges, ax=ax, histtype='fill', color='red', label=f'H2M-{device_name}', flow="sum")

# Set labels and title
ax.set_xlabel("Single pixel noise [e-]")
ax.set_ylabel("Number of Pixels")

# Log scale on y-axis
# ax.set_yscale('log')

# Add a legend
ax.legend()

# Limit x-axis range
ax.set_xlim(0, 6*calibration_factor)

# Compute overflow bin
overflow = histogram.values()[-1]

# Get mean and standard deviation from the histogram
mean = histogram.values().mean() * calibration_factor
stddev = histogram.values().std() * calibration_factor

# use ROOT to get mean and std dev
import ROOT

# open the file with ROOT (sadly, uproot does not support GetMean and GetStdDev)
f = ROOT.TFile(file_path)
h = f.Get("noise")
mean = h.GetMean() * calibration_factor
stddev = h.GetStdDev() * calibration_factor
overflow = h.GetBinContent(h.GetNbinsX() + 1)
f.Close()
# leave ROOT hell after this brief stint

# Add text with the overflow bin in the top right corner below the legend
ax.text(0.95, 0.85, f"Bias Voltage: -{bias_voltage.replace('p', '.')},\nKrummenacher Current: {krummenacher_current}", transform=ax.transAxes, ha='right', va='top')
if calibrate:
    ax.text(0.95, 0.45, f"Mean: {mean:.2f} e-\nStandard Deviation: {stddev:.2f} e-", transform=ax.transAxes, ha='right', va='top')
else:
    ax.text(0.95, 0.45, f"Mean: {mean:.2f} DAC,\nStandard Deviation: {stddev:.2f} DAC\nOverflow: {int(overflow)} bins", transform=ax.transAxes, ha='right', va='top')
# Save the plot
plt.savefig(f"noise_H2M{device_name}_bias{bias_voltage}_{krummenacher_current}.png")

plt.close()
