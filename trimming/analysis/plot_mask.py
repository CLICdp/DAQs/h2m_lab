import pandas as pd
import sys

def count_masked_pixels(file_path):
    # Load the mask from a file
    headers = ['col', 'row', 'tp_enable', 'mask', 'tdac', 'not_top']
    mask = pd.read_csv(file_path, delimiter='\s+', header=None, names=headers, skiprows=1)

    # Convert the mask to a pandas DataFrame
    df = pd.DataFrame(mask)

    # Count the number of masked pixels
    masked_pixels = df['mask'].value_counts()

    number_of_masked_pixels = int(masked_pixels[1])

    # Print the DataFrame
    print(masked_pixels)
    return df, number_of_masked_pixels

# Get the file path from command-line argument
file_path = sys.argv[1]

# Call the function with the provided file path
df, number_of_masked_pixels = count_masked_pixels(file_path)

# Extract device name, bias voltage, and krummenacher current from the file path
try:
    import re
    match = re.search(r'H2M-(\w+)_([\dp]+V)_([\w\d]+)', file_path)
    device_name = match.group(1)
    bias_voltage = match.group(2)
    krummenacher_current = match.group(3)
except AttributeError:
    device_name = bias_voltage = krummenacher_current = "Unknown"

# make a 2D heatmap of the tdac values
import matplotlib.pyplot as plt
import seaborn as sns

# Set tdac value for masked pixels to NaN
df.loc[df['mask'] == 1, 'tdac'] = None

# Create a 2D histogram of the tdac values
heatmap = df.pivot(index='row', columns='col', values='tdac')

# Create a figure and axes
fig, ax = plt.subplots()

# Create a heatmap
sns.heatmap(heatmap, ax=ax, cmap='viridis')

# Set labels and title
ax.set_xlabel("Column")
ax.set_ylabel("Row")
ax.set_title(f"H2M-{device_name} -{bias_voltage.replace('p', '.')} bias, {krummenacher_current}: {number_of_masked_pixels} masked pixel")

# Save the figure
plt.savefig(f"heatmap_H2M{device_name}_bias{bias_voltage}_{krummenacher_current}.png")
