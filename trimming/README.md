# Trimming

## Setup

To prepare for the trimming analysis, you need to install ROOT. The recommended way is to install ROOT with conda, follow these steps:

1. Execute the script to install conda and ROOT:

```bash
source setup_conda.sh
```

2. Follow the installation process

3. Once the installation is complete, you can start using ROOT by activating the conda environment. The script is set up in a way that you can execute it and it will not install again conda but set up the environment.

```bash
source setup_conda.sh

## equivalent with
# source conda/bin/activate
# conda activate h2m
```

4. You can now use ROOT for your data analysis and visualization needs.


## Get data
To get the distributions for the 16 different DACs:

1. If not already present on the Caribou board, copy over the script and the config file:

```bash
# on laptop/computer
scp -r peary/config/ <zynq>:~/<DEVICE_ID>/
scp -r peary/scripts/ <zynq>:~/<DEVICE_ID>/
scp peary/get_trimming_data.sh <zynq>:~/<DEVICE_ID>/
scp peary/get_tuned_data.sh <zynq>:~/<DEVICE_ID>/
```

2. Run the script to obtain the trimming data with the suitable config file
```bash
# on zynq board
bash get_trimming_data.sh
```

This will produce the ```noise_scan_0.bin, noise_scan_1.bin, ..., noise_scan_14``` data files.

Note, that ```.bin``` files are appended and not overwritten, therefore the script cleans up in the beginning.

## Analysis script
For the analysis, you can use the code in `analysis/`.

1. If you have not done so, copy the results of the noise scan over, e.g. to a directory called `workdir`.

```bash
# on laptop/computer

# copy data from caribou with H2M to directory
scp "<zynq>:~/<DEVICE_ID>/noise_scan_*.bin" workdir/
```

You can alternatively modify the `DEVICE` line in `scripts/get_scan.sh` and execute this convenience script.

2. Move to the folder when you have ```noise_scan_*.bin``` files. Modify the path to ``` H2M_scanReader.C```  and ```createTrimming.C```. And run:

```bash
# on laptop/computer
python3 run_analysis.py
```
The script uses H2M_scanReader.C to create root-files in data directory with single pixel plots and fit results. To create the mask

```bash
# on laptop/computer
# trimming_target: H2M-5: 92, H2M-4: 95, H2M-3: 95, H2M-2: 99
root -l -x -b analysis/createTrimming.C\(95\) # you need to modify the trimming target
```

A mask file is created.

You can alternatively modify the `DEVICE` line in `scripts/run_analysis.sh` and execute this convenience script.
It will also copy the masks to the H2M board.


## Get data with your trimming dacs mask

1. From the previous step you should have obtained the mask as a file called `tuned.mask`.
Copy the file to the Caribou board.

```bash
# on laptop/computer
scp tuned.mask <zynq>:~/<DEVICE_ID>/masks/

```

2. Then run with the configuration file which sets up the mask and the script to run a single parameter scan


```bash
# on zynq board
bash get_tuned_data.sh
```

You will get ```noise_scan_tuned.bin```. Copy it to your computer.

```bash
# on laptop/computer
scp <zynq>:~/<DEVICE_ID>/noise_scan_tuned.bin .
```


3. Plot the trimmed distribution for the noise scan with the equalised matrix applied

```bash
root -l -x 'analysis/H2M_scanReader.C("tuned_result.bin",50)'
```

4. Run ```analysis/plotting.py``` will plot the lowest, highest and tuned distributions together.
