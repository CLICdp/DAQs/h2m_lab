#!/bin/bash
DEVICE=H2M-4
scp -O labzynq:~/${DEVICE}/tuned_result.bin workdir/
scp -O labzynq:~/${DEVICE}/tuned_masked_result.bin workdir/
root -l -x 'analysis/H2M_scanReader.C("workdir/tuned_result.bin",50)'
root -l -x 'analysis/H2M_scanReader.C("workdir/tuned_masked_result.bin",50)'
python3 analysis/plot_tuned.py
python3 analysis/plot_noise.py workdir/tuned_masked_result.root