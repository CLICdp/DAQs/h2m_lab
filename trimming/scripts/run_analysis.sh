#!/bin/bash
DEVICE=H2M-4
python3 run_analysis.py
# trimming_target: H2M-5: 92, H2M-4: 95, H2M-3: 95, H2M-2: 99
root -l -x -b analysis/createTrimming.C\(95\)

# scp -O workdir/tuned.mask labzynq:/home/root/${DEVICE}/masks/
# scp -O workdir/tuned_masked.mask labzynq:/home/root/${DEVICE}/masks/

# python analysis/plot_mask.py workdir/tuned_masked.mask